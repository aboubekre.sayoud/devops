#------------------------------------------------------------------------------------ 
# Terraform CHANGELOG : https://github.com/hashicorp/terraform/blob/main/CHANGELOG.md
# Provider AWS CHANGELOG : https://github.com/hashicorp/terraform-provider-aws/blob/main/CHANGELOG.md
#------------------------------------------------------------------------------------
terraform {
  required_version = "1.4.2"
  required_providers {
    aws = "4.63.0"
  }
}

provider "aws" {
  region = "us-east-1"
}

#
# Initialisation du backend distant
# https://developer.hashicorp.com/terraform/language/settings/backends/s3
#
terraform {
  backend "s3" {
    region         = "us-east-1"
    bucket         = "aws-lockid"
    key            = "LockID"
    dynamodb_table = "aws-dynamoName"
  }
}
