variable "region" {
  description = "Region used for deploying on AWS."
  type        = string
  default     = "us-east-1"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "iam_instance_profile" {
  default = "arn:aws:iam::605776579979:instance-profile/LabInstanceProfile"
}

variable "tags" {
  description = "Tags mappings"
  type        = map(any)
  default = {
    SCHOOL = "EPITA",
    PROMO  = "APPING I 2021-2024"
    GROUP  = "1"
  }
}